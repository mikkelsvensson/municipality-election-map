# import plotly.graph_objects as go
# fig = go.Figure(data=go.Bar(y=[2, 3, 1]))
# fig.write_html('first_figure.html', auto_open=True)

# import plotly.express as px
# df = px.data.gapminder().query("year==2007")
# fig = px.scatter_geo(df, locations="iso_alpha", color="continent",
#                      hover_name="country", size="pop",
#                      projection="natural earth")
# fig.write_html('first_figure.html', auto_open=True)

# import plotly.graph_objects as go

# fig = go.Figure(go.Scattergeo())
# fig.update_layout(height=300, margin={"r":0,"t":0,"l":0,"b":0})
# fig.show()

# import pandas as pd
# us_cities = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/us-cities-top-1k.csv")
# print(us_cities)
# import plotly.express as px

# fig = px.scatter_mapbox(us_cities, lat="lat", lon="lon", hover_name="City", hover_data=["State", "Population"],
#                         color_discrete_sequence=["fuchsia"], zoom=3, height=300)
# fig.update_layout(mapbox_style="open-street-map")
# fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
# fig.show()

# import plotly.express as px
# df = px.data.tips()
# print(df)
# df["size"] = df["size"].astype(str)
# fig = px.scatter(df, x="total_bill", y="tip", color="size",
#                  title="String 'size' values mean discrete colors")
# print(df)
# #fig.show()

import plotly.graph_objects as go

fig = go.Figure(go.Scattergeo())
fig.update_geos(
    visible=False, resolution=50,
    showcountries=True, countrycolor="RebeccaPurple"
)
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()