from numpy import NaN
import requests
import pandas as pd
from io import StringIO
import plotly.express as px

CSV_URL_API = 'https://api.statbank.dk/v1/data/VALGK3/CSV?lang=en&valuePresentation=Value&timeOrder=Ascending&allowVariablesInHead=true&Tid=*&OMR%C3%85DE=*&PARTI=*&STEMMER(Head,EliminateByValue)=1'
api_data = requests.get(CSV_URL_API)
api_df = pd.read_csv(StringIO(api_data.text), sep=";")

CSV_URL_LAT_LOG = 'https://gitlab.com/mikkelsvensson/municipality-election-map/-/raw/main/kommuner_denmark_lat_log.csv?inline=false'
coordinate_data = requests.get(CSV_URL_LAT_LOG)
coordinate_df = pd.read_csv(StringIO(coordinate_data.text), sep=";")

combined_df=api_df
combined_df.insert(len(api_df.columns),'lat',None)
combined_df.insert(len(api_df.columns),'lon',None)
combined_df.insert(len(api_df.columns),'total',None)
combined_df.insert(len(api_df.columns),'relative',None)

def lat(city):
    return float(coordinate_df[coordinate_df["OMRÅDE"].str.contains(city)]['lat'])
def lon(city):
    return float(coordinate_df[coordinate_df["OMRÅDE"].str.contains(city)]['lon'])

combined_df.drop(combined_df[combined_df["OMRÅDE"].str.contains("Region")].index, inplace = True)
combined_df.drop(combined_df[combined_df["OMRÅDE"].str.contains("All Denmark")].index, inplace = True)
combined_df.drop(combined_df[combined_df["OMRÅDE"].str.contains("Christiansø")].index, inplace = True)

for municipality in combined_df["OMRÅDE"].unique():
    combined_df.loc[combined_df[combined_df["OMRÅDE"].str.contains(municipality)].index,'lat']=lat(municipality)
    combined_df.loc[combined_df[combined_df["OMRÅDE"].str.contains(municipality)].index,'lon']=lon(municipality)
    #print(combined_df[combined_df["OMRÅDE"].str.contains(municipality)])
    #print(combined_df.loc[combined_df[combined_df["OMRÅDE"].str.contains(municipality)],'INDHOLD'])



parties=combined_df["PARTI"].unique()
#print(combined_df)

combined_df=combined_df.set_index(['OMRÅDE','TID'])

for city, year in combined_df.index.unique():
    sum=combined_df.loc[(city,year),'INDHOLD'].sum()
    combined_df.loc[(city,year),'total']=sum


combined_df['relative']=combined_df['INDHOLD']/combined_df['total']*100
combined_df=combined_df.reset_index()
combined_df=combined_df.convert_dtypes(convert_floating=False)
#PLOT------------------------------------------------------------------------------------------------------
fig = px.scatter_mapbox(combined_df, lat="lat", lon="lon", hover_name='OMRÅDE', size="relative",size_max=35, hover_data=["PARTI"], color="PARTI",animation_frame='TID', zoom=6, color_discrete_map={parties[0]: "#C8032C",parties[1]: "#ED008D",parties[2]: "#004931",parties[3]: "#00505A",parties[4]: "#A30000",parties[5]: "#002945",parties[6]: "#EAC73E",parties[7]: "#F5BF39",parties[8]: "#000080",parties[9]: "#D1004C",parties[10]: "#00FF00",parties[11]: '#808080'})
fig.update_layout(mapbox_style="open-street-map")
fig.update_layout(margin={"r":50,"t":50,"l":50,"b":50})
fig.update_layout(title_text = f'Municipality Election Denmark (Click legend to toggle traces)',showlegend = True)
fig.write_html('first_figure.html')
fig.show()